<?php

use Bittacora\Blog\Http\Controllers\BlogController;
use Bittacora\Blog\Http\Controllers\PublicBlogController;
use Bittacora\Blog\Models\BlogModel;
use Bittacora\Category\CategoryFacade;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel')->middleware(['web', 'auth','admin-menu'])->name('blog.')->group(function(){
    Route::get('blog', [BlogController::class, 'index'])->name('index');
    Route::get('blog/{blog}/show', [BlogController::class, 'show'])->name('show');
    Route::get('blog/create/language/{locale?}', [BlogController::class, 'create'])->name('create');
    Route::post('blog/store', [BlogController::class, 'store'])->name('store');
    Route::get('blog/{model}/edit/language/{locale?}', [BlogController::class, 'edit'])->name('edit');
    Route::put('blog/{model}/update/language/{locale?}', [BlogController::class, 'update'])->name('update');
    Route::delete('blog/{blog}', [BlogController::class, 'destroy'])->name('destroy');
    Route::post('blog/reorder', [BlogController::class, 'reorder'])->name('reorder');
    Route::get('blog/multimedia', [BlogController::class, 'multimedia'])->name('multimedia');
    CategoryFacade::addRoutes('blog', BlogModel::class, BlogController::class);
});


Route::middleware(['web'])->name('publicBlog.')->group(function(){
    Route::get('blog', [PublicBlogController::class, 'index'])->name('index');
    Route::get('blog/{slug}', [PublicBlogController::class, 'show'])->name('show');
});
