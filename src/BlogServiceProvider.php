<?php

namespace Bittacora\Blog;

use Bittacora\Blog\Http\Livewire\BlogDatatable;
use Bittacora\Blog\Models\BlogModel;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Blog\Commands\InstallCommand;

class BlogServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('blog')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_blog_table')
            ->hasCommand(InstallCommand::class);
    }

    public function register(){
        $this->app->bind('blog', function($app){
            return new Blog();
        });

        /*
         * Create aliases for the dependency.
         */
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('BlogModel', BlogModel::class);

    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ .'/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'blog');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'blog');
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class
            ]);
        }

        Livewire::component('blog::blog-datatable', BlogDatatable::class);
    }
}
