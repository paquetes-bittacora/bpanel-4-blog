<?php

namespace Bittacora\Blog\Models;

use Bittacora\Category\Models\CategorizableModel;
use Bittacora\Category\Models\CategoryModel;
use Bittacora\Category\Traits\HasCategorizable;
use Bittacora\Content\Models\ContentModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;
use Wildside\Userstamps\Userstamps;
use Illuminate\Database\Eloquent\SoftDeletes;
use Bittacora\ContentMultimedia\ContentMultimediaFacade;

class BlogModel extends Model implements Sortable
{
    use HasTranslations;
    use SortableTrait;
    use Userstamps;
    use HasTranslatableSlug;
    use SoftDeletes;
//    use HasCategorizable;

    public $sortable = [
        'sort_when_creating' => true
    ];

    protected $fillable = [
        'title',
        'subtitle',
        'short_text',
        'long_text',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'active',
        'start_date',
        'end_date',
        'featured'
    ];

    public $translatable = [
        'title',
        'subtitle',
        'short_text',
        'long_text',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    protected $table = 'blogs';

    protected $with = ['content'];

    public function content()
    {
        return $this->morphOne(ContentModel::class, 'model');
    }

    public function categorizable()
    {
        return $this->morphOne(CategorizableModel::class, 'categorizable');
    }


    public function getSlugOptions(): SlugOptions
    {
//        return SlugOptions::create()
//            ->generateSlugsFrom('title')
//            ->saveSlugsTo('slug')
//            ->usingLanguage($this->getLocale());


        return SlugOptions::createWithLocales([$this->getLocale()])
            ->generateSlugsFrom(function($model, $locale) {
                return "{$model->title}";
            })
            ->saveSlugsTo('slug');
    }

    public function setStartDateAttribute($value){
        $this->attributes['start_date'] = Carbon::createFromFormat('d/m/Y H:i', $value)->format('Y-m-d H:i');
    }

    public function getStartDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i');
    }

    public function setEndDateAttribute($value){
        if(!is_null($value)){
            $this->attributes['end_date'] = Carbon::createFromFormat('d/m/Y H:i', $value)->format('Y-m-d H:i');
        }
    }

    public function getEndDateAttribute($value)
    {
        if(!is_null($value)){
            return Carbon::parse($value)->format('d-m-Y H:i');
        }
    }

    public function getImages(): array
    {
        $images = ContentMultimediaFacade::retrieveContentImages('blog', $this->content->id);
        if (empty ($images)) {
            return [];
        }
        return $images[0]->media()->get()->all();
    }
}
