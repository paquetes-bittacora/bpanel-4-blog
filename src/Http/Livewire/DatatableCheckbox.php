<?php

namespace Bittacora\Blog\Http\Livewire;

use Bittacora\Blog\Models\BlogModel;
use Livewire\Component;

class DatatableCheckbox extends Component
{
    public $name;
    public $idField;
    public $value = null;
    public $checked = false;
    public $multiple = false;
    public $title = null;
    public $labelText;
    public $blog;


    public function mount(BlogModel $blog){
        $this->blog = $blog;
    }

    public function render()
    {
        return view('blog::livewire.datatable-checkbox')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'checked' => $this->checked,
            'multiple' => $this->multiple,
            'labelText' => $this->labelText,
        ]);
    }

    public function toggle(BlogModel $blogModel){
        dd("hola2);");
        $blogModel->update(['active' => !$blogModel->active]);
        $this->checked = $blogModel->active;
    }
}
