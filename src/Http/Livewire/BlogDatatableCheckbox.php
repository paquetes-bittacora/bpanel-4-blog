<?php

namespace Bittacora\Blog\Http\Livewire;

use Livewire\Component;
use function view;

class BlogDatatableCheckbox extends Component
{

    public string $size;
    public string $permission;
    public string $property;
    public string $model;

    public function render()
    {
        return view('blog::livewire.blog-datatable-checkbox');
    }

    public function toggle(){
        dd("hola");
    }
}
