<?php

namespace Bittacora\Blog\Http\Livewire;

use Bittacora\Blog\Models\BlogModel;
use Bittacora\Content\ContentFacade;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class BlogDatatable extends DataTableComponent
{

    public int $contentId;
    public bool $reordering = false;
    public bool $showPerPage = true;
    public bool $showPagination = true;
    public bool $showSearch = true;
    public array $perPageAccepted = [10,25,50,100];
    public string $emptyMessage = "El registro de entradas está vacío";


//    protected $listeners = ['refreshContentMultimediaImagesWidgetTable' => '$refresh'];

    public function columns(): array
    {
        return [
            Column::make('Título', 'title')->addClass('w-50'),
            Column::make('Idiomas')->addClass('w-10 text-center'),
            Column::make('Activo', 'active')->addClass('w-10 text-center'),
            Column::make('Destacado', 'featured')->addClass('w-10 text-center'),
            Column::make('Fecha de publicación', 'start_date')->addClass('w-10 text-center'),
            Column::make('Acciones')->addClass('w-10')
        ];
    }

    public function query()
    {
        return BlogModel::query()->orderBy('start_date', 'DESC')->
        when($this->getFilter('search'), fn ($query, $term) => $query->where('title->es', 'like', '%'.strtoupper($term).'%')
            ->orWhere('title->es', 'like', '%'.strtolower($term).'%')->orWhere('title->es', 'like', '%'.ucfirst($term).'%'));
    }

    public function rowView(): string
    {
        return 'blog::livewire.blog-datatable';
    }

    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar'
        ];
    }

    public function bulkDelete(){
        if(count($this->selectedKeys())){
            foreach($this->selectedKeys() as $key){
                $blog = BlogModel::where('id', $key) -> first();
                $blog->delete();
                ContentFacade::deleteModelContent($blog);
            }
            $this->resetAll();
        }
    }

}
