<?php


namespace Bittacora\Blog\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Category\CategoryFacade;
use Bittacora\Category\Models\CategoryModel;
use Bittacora\Category\Traits\CategoryController;
use Bittacora\Content\ContentFacade;
use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Bittacora\Language\LanguageFacade;
use Bittacora\Multimedia\Models\Multimedia;
use Bittacora\Multimedia\MultimediaFacade;
use Bittacora\Blog\Http\Requests\StoreBlogRequest;
use Bittacora\Blog\Http\Requests\UpdateBlogRequest;
use Bittacora\Blog\Models\BlogModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    use CategoryController;

    public function create($language = null)
    {
        $this->authorize('blog.create');

        if (empty($language)) {
            $defaultLanguage = LanguageFacade::getDefault();
            $language = $defaultLanguage->locale;
        }

        $allBlogs = BlogModel::orderBy('start_date', 'DESC') ->pluck('title', 'id') -> toArray();

        return view('blog::create', ['language' => $language, 'allowedFormats' => Multimedia::$allowedFormats, 'allowedExtensions' => Multimedia::$allowedExtensions,
            'categories' => CategoryModel::where('model', BlogModel::class)->pluck('title','id')->toArray(), 'allBlogs' => $allBlogs]);
    }

    public function edit(BlogModel $model, $language = null)
    {
        $this->authorize('blog.edit');
        if (empty($language)) {
            $language = LanguageFacade::getDefault()->locale;
        }

        $allowedFormats = Multimedia::$allowedFormats;
        $allowedExtensions = Multimedia::$allowedExtensions;
        $allBlogs = BlogModel::where('id', '!=', $model->id) -> orderBy('start_date', 'DESC') ->pluck('title', 'id') -> toArray();
        $relatedBlogs = DB::table('blog_related')->where('blog_id', $model->id)->pluck('related_id')->toArray();
        $model->setLocale($language);
        return view('blog::edit', ['language' => $language, 'blog' => $model, 'multimedia' => MultimediaFacade::getAll(),
            'allowedFormats' => $allowedFormats, 'allowedExtensions' => $allowedExtensions, 'allBlogs' => $allBlogs,
            'relatedBlogs' => $relatedBlogs, 'categories' => CategoryModel::where('model', BlogModel::class)->pluck('title','id')->toArray()]);
    }

    public function index()
    {
        $this->authorize('blog.index');
        return view('blog::index');
    }

    /**
     * @param Request $request
     */
    public function reorder(Request $request)
    {
        $collect = collect($request->json()->all());
        BlogModel::setNewOrder($collect->pluck('id'), $collect->first()['position']);
    }

    public function show(BlogModel $blog)
    {
        $locale = LanguageFacade::getDefault()->locale;
        return redirect(route('blog-public.show', [
            'locale' => $locale,
            'slug' => $blog->slug
        ]));
    }

    public function store(StoreBlogRequest $request)
    {
        $this->authorize('blog.store');

        $blog = new BlogModel();
        $blog->setLocale($request->input('locale'));

        $blog->fill($request->all());
        $blog->save();

        ContentFacade::associateWithModel($blog);
        if(!is_null($request->input('category'))){
            CategoryFacade::associateWithCategory($blog, $request->input('category'));
        }
        if(!empty($_FILES['file']['name'][0])){
            ContentFacade::associateWithMultimedia($blog, $_FILES['file']);
        }

        return redirect()->route('blog.index')->with(['alert-success' => __('blog::blog.created')]);
    }

    public function update(UpdateBlogRequest $request, BlogModel $model, $locale)
    {
        $this->authorize('blog.update');
        $relatedBlogs = $request->input('relatedBlogs');
        $request->request->remove('relatedBlogs');
        $model->setLocale($request->input('locale'));

        $model->fill($request->all());
        $model->save();

        ContentFacade::associateWithModel($model);
        if(is_null($request->input('category'))){
            CategoryFacade::deleteModelCategorizable($model);
        }else{
            CategoryFacade::associateWithCategory($model, $request->input('category'));
        }
        if(!empty($_FILES['file']['name'][0])){
            ContentFacade::associateWithMultimedia($model, $_FILES['file']);
        }

        DB::table('blog_related')->where('blog_id', $model->id)->delete();

        if(!empty($relatedBlogs)){
            foreach($relatedBlogs as $relatedBlog){
                DB::table('blog_related')->insert([
                    'blog_id' => $model->id,
                    'related_id' => $relatedBlog
                ]);
            }
        }

        return redirect()->route('blog.index')->with(['alert-success' => __('blog::blog.updated')]);
    }

    public function destroy(BlogModel $blog)
    {
        $this->authorize('blog.destroy');

        if ($blog->delete()) {
            // Reordeno después de borrar
            $ids = BlogModel::ordered()->pluck('id');
            BlogModel::setNewOrder($ids);

            // Borro el content
            ContentFacade::deleteModelContent($blog);
            CategoryFacade::deleteModelCategorizable($blog);

            Session::put('alert-success', __('blog::blog.deleted'));
        } else {
            Session::put('alert-warning', __('blog::blog.not-deleted'));
        }
        return redirect(route('blog.index'));
    }

    public function showPublic($slug)
    {
        $currentDate = Carbon::now()->format('Y-m-d H:i');
        $blog = BlogModel::where('slug->'.app()->getLocale(), $slug)->where('active', 1)->where('start_date', '<=', $currentDate)->with('content')->firstOrFail();
        return view('blog-entry', compact('blog'));
    }
}
