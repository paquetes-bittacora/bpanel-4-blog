<?php

namespace Bittacora\Blog\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Blog\BlogFacade;
use Bittacora\Blog\Models\BlogModel;
use Bittacora\ContentMultimedia\ContentMultimediaFacade;

class PublicBlogController extends Controller
{
    public function index(){
        $blogEntries = BlogModel::orderBy('start_date', 'DESC')->with('content')->paginate(12)->withPath('/blog');
        foreach($blogEntries as $key => $blog){
            $blogEntries[$key]->images = ContentMultimediaFacade::retrieveContentImages('blog', $blog->content->id);
        }

        return view('blog', compact('blogEntries'));
    }

    public function show($slug){
        $blogEntry = BlogModel::where('slug->'.app()->getLocale(), $slug)->with('content')->first();
        $blogEntry->images = ContentMultimediaFacade::retrieveContentImages('blog', $blogEntry->content->id);
        $blogEntry->relatedBlogs = BlogFacade::retrieveBlogsRelated($blogEntry->id);
        return view('blog-entry', compact('blogEntry'));
    }
}
