<?php

namespace Bittacora\Blog\Http\Requests;

use Bittacora\Seo\SeoFacade;
use Illuminate\Foundation\Http\FormRequest;

class StoreBlogRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        if($this->has('active')){
            $this->request->add(['active' => 1]);
        }else{
            $this->request->add(['active' => 0]);
        }

        if($this->has('featured')){
            $this->request->add(['featured' => 1]);
        }else{
            $this->request->add(['featured' => 0]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:255',
            'end_date' => 'nullable'
        ];

        $rules = SeoFacade::addRequestValidationRules($rules, 'blogs');

        return $rules;
    }
}
