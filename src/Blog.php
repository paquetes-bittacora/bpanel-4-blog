<?php

namespace Bittacora\Blog;

use Bittacora\Blog\Models\BlogModel;
use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Illuminate\Support\Facades\DB;

class Blog
{
    public function retrieveBlogsRelated($blogId){
        $blogsRelatedIds = DB::table('blog_related')->where('blog_id', $blogId)->pluck('related_id')->toArray();
        $blogsRelated = BlogModel::whereIn('id', $blogsRelatedIds)->get();
        foreach($blogsRelated as $key => $value){
            $blogsRelated[$key]->images = ContentMultimediaFacade::retrieveContentImages('blog', $value->content->id);
        }

        return $blogsRelated;
    }

    public function getFeatured(){
        $blogFeatured = BlogModel::where('featured', 1) -> where('active', 1) -> whereNotNull('title->'.app()->getLocale()) -> orderBy('start_date', 'DESC') -> get();
        if(is_null($blogFeatured)){
            return null;
        }
        foreach($blogFeatured as $key => $value){
            $blogFeatured[$key]->images = ContentMultimediaFacade::retrieveContentImages('blog', $value->content->id);
        }

        return $blogFeatured;
    }

    public function mostRecentEntries(){
        $blogMostRecentEntries = BlogModel::where([['active', '=', 1], ['featured', '!=', 1]]) -> orderBy('start_date', 'DESC') ->limit(3) -> get();
        return $blogMostRecentEntries;
    }

    // SEARCH

    public function search($term){
        $blog = BlogModel::where('title->es', 'like', '%'.strtoupper($term).'%')
            ->orWhere('title->es', 'like', '%'.strtolower($term).'%')->orWhere('title->es', 'like', '%'.ucfirst($term).'%')
            ->where('active', 1)->where('start_date', '<=', date('Y-m-d H:i:s'))->orderBy('start_date', 'DESC')->get();
        return $blog;
    }
}
