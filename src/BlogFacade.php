<?php

namespace Bittacora\Blog;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Blog\Blog
 */
class BlogFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'blog';
    }
}
