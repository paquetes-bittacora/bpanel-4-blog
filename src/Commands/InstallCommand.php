<?php

namespace Bittacora\Blog\Commands;

use Bittacora\AdminMenu\AdminMenuFacade;
use Bittacora\Category\CategoryFacade;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InstallCommand extends Command
{
    public $signature = 'blog:install';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('Registrando elementos del menú...');
        $module = AdminMenuFacade::createModule('contents', 'blog', 'Blog', 'fa fa-newspaper');
        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');
        AdminMenuFacade::createAction($module->key, 'Categorías', 'category', 'fa fa-sitemap');

        $this->comment('Hecho');

        $this->comment('Dando permisos al administrador...');
        $permissions = ['index', 'create', 'show', 'edit', 'destroy', 'store', 'update'];


        $adminRole = Role::findOrCreate('admin');
        foreach($permissions as $permission){
            $permission = Permission::firstOrCreate(['name' => 'blog.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }

//        CategoryFacade::addPermission('blog');
        $this->comment('Hecho');
    }
}
