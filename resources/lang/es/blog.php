<?php

return [
    'blog_list' => 'Listado de entradas',
    'title' => 'Título',
    'subtitle' => 'Subtítulo',
    'long_text' => 'Texto completo',
    'short_text' => 'Texto resumen',
    'add' => 'Crear entrada',
    'create_default_language' => '<i class="fas fa-info"></i>&nbsp;&nbsp;Al crear una nueva entrada, se creará en el idioma principal. Si desea añadir traducciones, guarde primero la entrada en el idioma principal.',
    'created' => 'La entrada fue creada correctamente.',
    'updated' => 'La entrada fue actualizada correctamente.',
    'deleted' => 'La entrada fue borrada correctamente.',
    'not-deleted' => 'No se pudo borrar la entrada.',
    'active' => 'Publicada',
    'edit' => 'Editar entrada',
    'show' => 'Mostrar entrada',
    'create' => 'Crear entrada',
    'index' => 'Listar entradas',
    'destroy' => 'Eliminar entrada',
    'store' => 'Añadir entrada',
    'update' => 'Actualizar entrada',
    'featured' => 'Destacado',
    'category_list' => 'Listado de categorías'
];
