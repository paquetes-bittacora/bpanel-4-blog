<?php

return [
    'index' => 'Listado',
    'blog'=> 'Blog',
    'create' => 'Nueva entrada',
    'edit' => 'Editar entrada',
    'category' => 'Categorías',
    'createCategory' => 'Crear categoría',
    'editCategory' => 'Editar categoría'
];
