<td>
    {{$row->title}}
</td>
<td>
    @livewire('language::datatable-languages', ['model' => $row, 'createRouteName' => 'blog.create', 'editRouteName' => 'blog.edit'], key('languages-blog-'.$row->id))
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-blog-'.$row->id))
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'featured', 'model' => $row, 'value' => $row->featured, 'size' => 'xxs'], key('featured-blog-'.$row->id))
</td>
<td>
    <div class="text-center">
        {{$row->start_date}}
    </div>
</td>
<td>
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'blog', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'la entrada?'], key('blog-buttons-'.$row->id))
</td>
