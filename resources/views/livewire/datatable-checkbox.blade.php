<div class="form-check form-check-inline">
    <input class="form-check-input ace-switch input-sm"
           type="checkbox"
           @if($multiple) name="{{$name}}[]" @else name="{{$name}}" @endif
           id="{{$idField}}"
           wire:click="toggle({{$idField}})"
           @if($checked==1) checked @endif
    >
    @if(!is_null($labelText))
        <label class="form-check-label" for="{{$idField}}">{{ $labelText }}</label>
    @endif
</div>
