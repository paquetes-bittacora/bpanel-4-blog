<div>
    <div class="text-center">
        @if ($value==0)
            <a class="btn btn-danger btn-{{$size}} mb-1 @cannot($permission) disabled @endcannot"
               wire:click="toggle()"
               wire:loading.class="btn-warning"
               title="Desactivado @cannot($permission) - Sin permiso @endcannot">
                <i class="fa fa-times" wire:loading.remove></i>
                <div wire:loading>
                    <i class="fa fa-cog fa-spin fa-fw"></i>
                </div>
            </a>
        @else
            <a class="btn btn-success btn-{{$size}} mb-1
            @cannot($permission) disabled @endcannot"
               wire:click="toggle()"
               wire:loading.class="btn-warning"
               title="Activado @cannot($permission) - Sin permiso @endcannot">
                <i class="fa fa-check" wire:loading.remove></i>
                <div wire:loading>
                    <i class="fa fa-cog fa-spin fa-fw"></i>
                </div>
            </a>
        @endif
    </div>
</div>
