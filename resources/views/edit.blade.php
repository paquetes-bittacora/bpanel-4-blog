@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Editar entrada')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('blog::blog.edit') }}</span>
            </h4>
            @include('language::partials.form-languages', ['model' => $blog, 'edit_route_name' => 'blog.edit',  'currentLanguage' => $language])
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ route('blog.update', ['model' => $blog, 'locale' => $language]) }}" enctype="multipart/form-data">
            @csrf

            @livewire('form::input-date', ['required' => true, 'labelText' => 'Fecha de inicio', 'name' => 'start_date', 'idField' => 'start_date', 'enableTime' => true, 'fieldWidth' => 3, 'value' => $blog->start_date])
            @livewire('form::input-date', ['labelText' => 'Fecha de fin', 'name' => 'end_date', 'idField' => 'end_date', 'enableTime' => true, 'fieldWidth' => 3, 'value' => $blog->end_date, 'defaultDate' => false])
            @livewire('form::input-text', ['name' => 'title', 'labelText' => __('blog::blog.title'), 'required'=>true, 'value' => $blog->title])
{{--            @livewire('form::input-text', ['name' => 'subtitle', 'labelText' => __('blog::blog.subtitle'), 'required'=>false, 'value' => $blog->subtitle])--}}
            @livewire('form::textarea', ['name' => 'short_text', 'labelText' => __('blog::blog.short_text'), 'required'=>false, 'value' => $blog->short_text])
            @livewire('utils::tinymce-editor', ['name' => 'long_text', 'labelText' => __('blog::blog.long_text'), 'value' => $blog->long_text])
            @if(!empty($allBlogs))
                @livewire('form::dual-list-box', ['name' => 'relatedBlogs[]', 'idField' => 'relatedBlogsSelect', 'labelText' => 'Noticias relacionadas',
                'allValues' => $allBlogs, 'height' => '232px', 'selectedValues' => $relatedBlogs])
            @endif
{{--            @if($blog->categorizable()->exists())--}}
{{--                @livewire('form::select', ['name' => 'category', 'allValues' => $categories, 'labelText' => __('blog::blog.category'), 'emptyValue' => true, 'emptyValueText' => 'Sin categoría', 'selectedValues' => [$blog->categorizable->category_id]])--}}
{{--            @else--}}
{{--                @livewire('form::select', ['name' => 'category', 'allValues' => $categories, 'labelText' => __('blog::blog.category'), 'emptyValue' => true, 'emptyValueText' => 'Sin categoría'])--}}
{{--            @endif--}}


            @livewire('content-multimedia::upload-content-multimedia-widget', ['model' => $blog, 'allowedFormats' => $allowedFormats, 'allowedExtensions' => $allowedExtensions])
            @livewire('content-multimedia-images::content-multimedia-images-widget', ['contentId' => $blog->content->id, 'module' => 'blog', 'permission' => 'blog.edit'])
{{--            @livewire('content-multimedia-documents::content-multimedia-documents-widget', ['contentId' => $blog->content->id, 'permission' => 'blog.edit'])--}}
{{--            @livewire('content-multimedia-links::content-multimedia-links-widget', ['contentId' => $blog->content->id, 'permission' => 'blog.edit'])--}}
            @livewire('seo::seo-fields', ['model' => $blog])
            @livewire('form::input-checkbox', [
                'name' => 'active',
                'value' => 1,
                'labelText' => __('blog::blog.active'),
                'bpanelForm' => true,
                'checked' => $blog->active == 1
            ])

            @livewire('form::input-checkbox', [
            'name' => 'featured',
            'value' => 1,
            'labelText' => __('blog::blog.featured'),
            'bpanelForm' => true,
            'checked' => $blog->featured == 1
            ])

            @livewire('utils::created-updated-info', ['model' => $blog])

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'save'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @livewire('form::input-hidden', ['name' => 'locale', 'value'=> $language])
            @livewire('form::input-hidden', ['name' => 'id', 'value'=> $blog->id])
            <input type="hidden" name="_method" value="PUT">
        </form>
    </div>

    @livewire('multimedia::multimedia-images-library')
@endsection
