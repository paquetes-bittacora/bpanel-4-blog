@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Crear entrada')

@section('content')
    <p class="alert alert-success">
        {!!  __('blog::blog.create_default_language') !!}
    </p>
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('blog::blog.add') }}</span>
            </h4>
            <img src="{{asset('img/flags/'.$language.'.png')}}" alt="{{ $language }}">
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('blog.store')}}" enctype="multipart/form-data">
            @csrf
            @livewire('form::input-date', ['required' => true, 'labelText' => 'Fecha de inicio', 'name' => 'start_date', 'idField' => 'start_date', 'enableTime' => true, 'fieldWidth' => 3, 'defaultDate' => true])
            @livewire('form::input-date', ['labelText' => 'Fecha de fin', 'name' => 'end_date', 'idField' => 'end_date', 'enableTime' => true, 'fieldWidth' => 3, 'defaultDate' => false])
            @livewire('form::input-text', ['name' => 'title', 'labelText' => __('blog::blog.title'), 'required'=>true])
            @livewire('form::input-text', ['name' => 'subtitle', 'labelText' => __('blog::blog.subtitle'), 'required'=>false])
            @livewire('form::textarea', ['name' => 'short_text', 'labelText' => __('blog::blog.short_text'), 'required'=>false])
            @livewire('utils::tinymce-editor', ['name' => 'long_text', 'labelText' => __('blog::blog.long_text')])
            @if(!empty($allBlogs))
                @livewire('form::dual-list-box', ['name' => 'relatedBlogs[]', 'idField' => 'relatedBlogsSelect', 'labelText' => 'Noticias relacionadas',
                'allValues' => $allBlogs, 'height' => '232px'])
            @endif
{{--            @livewire('form::select', ['name' => 'category', 'allValues' => $categories, 'labelText' => __('blog::blog.category'), 'placeholder' => 'Sin categoría'])--}}

            @livewire('content-multimedia::upload-content-multimedia-widget', ['model' => null, 'allowedFormats' => $allowedFormats, 'allowedExtensions' => $allowedExtensions])
            @livewire('seo::seo-fields')
            @livewire('form::input-checkbox', ['name' => 'active', 'value' => 1, 'checked' => true, 'labelText' => __('blog::blog.active'), 'bpanelForm' => true])
            @livewire('form::input-checkbox', ['name' => 'featured', 'value' => 1, 'labelText' => __('blog::blog.featured'), 'bpanelForm' => true])

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'save'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @livewire('form::input-hidden', ['name' => 'locale', 'value'=> $language])
        </form>
        @livewire('multimedia::multimedia-images-library')
    </div>
@endsection
