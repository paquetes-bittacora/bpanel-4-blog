<?php

declare(strict_types=1);

namespace Bittacora\Blog\Database\Seeders\Seeds;

use Bittacora\ContentMultimediaImages\ContentMultimediaImagesLocationFacade;
use Illuminate\Database\Seeder;

/**
 * Class PublicMenuSeeder
 * @package Bittacora\PublicMenu\Database\Seeders\Seeds
 */
class BlogContentMultimediaLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContentMultimediaImagesLocationFacade::createNewLocation('blog', 'Imagen destacada');
    }
}
