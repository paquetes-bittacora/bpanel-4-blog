<?php

declare(strict_types=1);

namespace Bittacora\Blog\Database\Seeders;

use Bittacora\Blog\Database\Seeders\Seeds\BlogContentMultimediaLocationSeeder;
use Bittacora\Category\CategoryFacade;
use Bittacora\Tabs\Tabs;
use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 * @package Bittacora\Blog\Database\Seeders
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            BlogContentMultimediaLocationSeeder::class
        ]);

        Tabs::createItem('blog.index', 'blog.index', 'blog.index', 'Noticias','fa fa-list');
        Tabs::createItem('blog.index', 'blog.create', 'blog.create', 'Nuevo','fa fa-plus');

        Tabs::createItem('blog.create', 'blog.index', 'blog.index', 'Noticias','fa fa-list');
        Tabs::createItem('blog.create', 'blog.create', 'blog.create', 'Nuevo','fa fa-plus');

        Tabs::createItem('blog.edit', 'blog.index', 'blog.index', 'Noticias','fa fa-list');
        Tabs::createItem('blog.edit', 'blog.create', 'blog.create', 'Nuevo','fa fa-plus');

        CategoryFacade::createTabs('blog', 'Noticias');
    }
}
