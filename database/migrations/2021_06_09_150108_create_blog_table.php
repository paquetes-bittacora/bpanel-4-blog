<?php

use Bittacora\Seo\SeoFacade;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->longText('title');
            $table->longText('subtitle')->nullable();
            $table->longText('short_text')->nullable();
            $table->longText('long_text')->nullable();
            SeoFacade::addDatabaseFields($table);
            $table->unsignedInteger('order_column')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('featured')->default(false);
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        // No llamamos a los seeders en los test porque los ralentizan
        // y no es necesario
        if (!App::runningUnitTests()) {
            Artisan::call('blog:install');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
